package com.stoliarenko.hibernate.demo.entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "event_company")
public class EventCompany {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "company_name")
    private String companyName;

    @Column(name = "email")
    private String email;

    @OneToMany(fetch = FetchType.LAZY,
                mappedBy = "companyId",
                cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
    private List<Event> events;

    public EventCompany() {
    }

    public EventCompany(String companyName, String email) {
        this.companyName = companyName;
        this.email = email;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<Event> getEvents() {
        return events;
    }

    public void setEvents(List<Event> events) {
        this.events = events;
    }

    public void add(Event event){
        if(events == null){
            events = new ArrayList<>();
        }

        events.add(event);
        event.setCompany_id(this);
    }

    @Override
    public String toString() {
        return "EventCompany{" +
                "id=" + id +
                ", companyName='" + companyName + '\'' +
                ", email='" + email +
                ", events=" + events +
                '}';
    }
}
