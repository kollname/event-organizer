package com.stoliarenko.hibernate.demo.entity;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "customer")
public class Customer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "email")
    private String email;

    @Column(name = "balance")
    private int balance;

    @Column(name = "sociability")
    private int sociability;

    @Column(name = "family_size")
    private int familySize;

    @Column(name = "extreem")
    private boolean extreem;

    @Column(name = "music")
    private boolean music;

    @Column(name = "creativity")
    private boolean creativity;

    @ManyToMany(fetch = FetchType.LAZY,
            cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
    @JoinTable(name = "event_customer",
                joinColumns = @JoinColumn(name = "customer_id"),
                inverseJoinColumns = @JoinColumn(name = "event_id"))
    private List<Event> events;

    public Customer() {
    }

    public Customer(String firstName, String lastName, String email, int balance, int sociability, int familySize, boolean extreem, boolean music, boolean creativity) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.balance = balance;
        this.sociability = sociability;
        this.familySize = familySize;
        this.extreem = extreem;
        this.music = music;
        this.creativity = creativity;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getBalance() {
        return balance;
    }

    public void setBalance(int balance) {
        this.balance = balance;
    }

    public int getSociability() {
        return sociability;
    }

    public void setSociability(int sociability) {
        this.sociability = sociability;
    }

    public int getFamilySize() {
        return familySize;
    }

    public void setFamilySize(int familySize) {
        this.familySize = familySize;
    }

    public boolean isExtreem() {
        return extreem;
    }

    public void setExtreem(boolean extreem) {
        this.extreem = extreem;
    }

    public boolean isMusic() {
        return music;
    }

    public void setMusic(boolean music) {
        this.music = music;
    }

    public boolean isCreativity() {
        return creativity;
    }

    public void setCreativity(boolean creativity) {
        this.creativity = creativity;
    }

    public List<Event> getEvents() {
        return events;
    }

//    public void addEvent (Event event){
//        if(events == null){
//            events = new ArrayList<>();
//        }
//
//        events.add(event);
//    }

    public void setEvents(List<Event> events) {
        this.events = events;
    }

    @Override
    public String toString() {
        return "Customer{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", email='" + email + '\'' +
                ", balance=" + balance +
                ", sociability=" + sociability +
                ", familySize=" + familySize +
                ", extreem=" + extreem +
                ", music=" + music +
                ", creativity=" + creativity +
                ", events=" + events +
                '}';
    }
}
