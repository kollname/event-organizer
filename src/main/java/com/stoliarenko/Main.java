package com.stoliarenko;


import com.stoliarenko.hibernate.demo.entity.Customer;
import com.stoliarenko.hibernate.demo.entity.Event;
import com.stoliarenko.hibernate.demo.entity.EventCompany;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;


public class Main {
    public static void main(String[] args) {

        SessionFactory factory = new Configuration()
                .configure("hibernate.cfg.xml")
                .addAnnotatedClass(EventCompany.class)
                .addAnnotatedClass(Customer.class)
                .addAnnotatedClass(Event.class)
                .buildSessionFactory();

        Session session = factory.getCurrentSession();

        try {
            session.beginTransaction();

//            EventCompany eventCompany = new EventCompany("Reading Sweaming pool", "sweam@reading.com");
//            EventCompany eventCompany2 = new EventCompany("Reading pool", "pool@reading.com");
//            System.out.println("Saved course " + eventCompany);
//            session.save(eventCompany);
//            session.save(eventCompany2);
//
//            Event event = new Event("Play guitar", "Just play", 0, eventCompany2, 1);
//            session.save(event);
//            Customer customer = new Customer("Nikolai", "Stoliarenko", "a@gmil.com", 23, 7, 2, true, true, true);
//            event.addCustomer(customer);

            Customer customer = session.get(Customer.class, 8);
            System.out.println("Custo events " + customer.getEvents());

//            session.save(customer);

//            session.getTransaction().commit();
            session.close();



        } finally {
            session.close();
            factory.close();
        }
    }
}
